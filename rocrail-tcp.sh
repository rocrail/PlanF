<rocrail planfile="plan-f.xml" libpath="." imgpath="images" createmodplan="false" occupation="occ.xml" ptiid="" lciid="" dpiid="" sviid="" sciid="" scsensor="" analyze="false" poweroffonexit="true" donkey="D1A0D991E9C4A4A599A69AA072A2A49CE48FD39AA4D3D7E7DEA3D3A1B6D7E1D6DED6D2DF6EE0D4D7" doneml="r.j.versluis@rocrail.net">
  <trace file="rocrail" protpath="./traces" debug="false" automatic="false" byte="false" parse="false" monitor="true" rfile="rocrail.trace" listen2all="false"/>
  <remark text=""/>
  <digint iid="ln-1" lib="loconet" cmdstn="dcs100" device="/dev/tty.PL2303-00001004" host="192.168.100.88" port="1235" swtime="250" bps="57600" sublib="tcp" stress="false" libpath="../Rocrail/unxbin" flow="cts" fastclock="true">
    <loconet purgetime="200" useidle="false" usefc="true" syncfc="false" ignorepowercmds="false" cmdstn="dcs100">
      <options store="true" opsw="0000010000000000000000000000100000000000001010000000000000000000"/>
      <slotserver active="false" lconly="true" purge="true" stopatpurge="false" iid=""/>
    </loconet>
  </digint>
  <ctrl loccnfg="true" minbklc="0" swtimeout="250" ignevt="5" initfieldpause="600" seed="25676" savepostime="10" check2in="false" secondnextblock="false" initfieldpower="false" enableswfb="true" eventtimeout="0" signalreset="0" routeswtime="10" disablerouteVreduce="false" greenaspect="true" defaspect="green" semaphorewait="1" skipsetsw="false" usebicom="true" poweroffonidentmismatch="false" poweroffatghost="true" signalwait="0" locostartgap="0" blockinitpause="0" useblockside="false" stopnonecommuter="false" skipsetsg="false" keepghost="false" disablesteal="false" poweroffatreset="true" allowzerothrottleid="false"/>
  <clock divider="1" hour="24" minute="60"/>
  <http port="53701" refresh="10">
    <webclient port="53702" refresh="10" webcampath="Webcam_Pictures" webcampos="right" symbolpath="symbols" symboltype="gif">
      <webcam title="Plan-F" imagefile="Webcam.jpeg"/>
    </webclient>
  </http>
  <tcp onlyfirstmaster="true" port="62842" maxcon="10"/>
  <r2rnet id="" addr="224.0.0.1" port="1234" routes="netroutes.xml" enable="false"/>
  <srcpcon port="4303" active="false"/>
</rocrail>
